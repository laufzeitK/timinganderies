
import numpy as np
from numpy import random as rand
from numpy import ma
import numpy.linalg as la

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.colors as cols

## from mpl_toolkits import mplot3d as plt3d

import sys, os

import warnings as warn
import multiprocessing as mp

import pickle
from useful import printit, loadData

from getopt import gnu_getopt as getopt

# import Vera's code, by now quite a bit modified
import models.AnderiesTim as anderies

# my own module
from myPhaseSpace import plotPhaseSpace, savePlot, saveAx



## mpl.rcParams["axes.labelsize"] = 36
mpl.rcParams["xtick.labelsize"] = 24
mpl.rcParams["ytick.labelsize"] = 24
mpl.rcParams["font.size"] = 36




#009FDA
styleFP = dict(markersize = 10, marker = "8", color = "#009FDA")
styleRefP = dict(markersize = 20, marker = "*", color = "#69923A")


## cdict = {'red': ((0., 0.89, 0.89),
##                  (1, 0, 0)),
##          'green': ((0., 0.45, 0.45),
##                    (1, 0.62, 0.62)),
##          'blue': ((0., 0.13, 0.13),
##                   (1, 0.85, 0.85))}
## 
## cdict = {'red': ((0., 0.89, 0.89),
##                  (1, 0.41, 0.41)),
##          'green': ((0., 0.45, 0.45),
##                    (1, 0.57, 0.57)),
##          'blue': ((0., 0.13, 0.13),
##                   (1, 0.23, 0.23))}
## 
## cdict = {'red': ((0, 0.41, 0.41),
##                  (1, 0, 0)),
##          'green': ((0, 0.57, 0.57),
##                    (1, 0.62, 0.62)),
##          'blue': ((0, 0.23, 0.23),
##                  (1, 0.85, 0.85))}
## 
## PIKcmap = cols.LinearSegmentedColormap('PIKcolormap',cdict,256, gamma = 0.6)
PIKcmap = plt.get_cmap("hot")

def pointIt(xy, style, ax = plt):
        ax.plot([xy[0]],[xy[1]], **style)

## def loadData(fname):
## 	printit("loading data from %s ..."%fname , end = " ")
## 	with open(fname, "rb") as f:
## 		data = pickle.load(f)
## 	printit("done")
## 	return data
## 

def plotData(x, y, alle, r0axs):


	assert len(r0axs) == len(alle)
	l = len(r0axs)
	for i, ax, xAll in zip(range(l), r0axs, alle):
		plt.sca(ax)
		addkwarg = {}
		if i in [0]: # where the absolute times are measured
			addkwarg["vmin"] = 0
			addkwarg["vmax"] = np.percentile(xAll, 100 - remPerc)
                elif i in [1]: # where the time difference is measured
			addkwarg["vmin"] = np.percentile(xAll, remPerc)
			addkwarg["vmax"] = np.percentile(xAll, 100 - remPerc)
## 			mask = xAll < -50
## 			xAll.mask[ mask ] = True
## 			print dir(xAll)
		c = plt.pcolor(x,y,xAll, cmap = PIKcmap, **addkwarg)
		
		cb = plt.colorbar()
		cb.outline.set_visible(False)
                cbs.append(cb)

                if i in [1]:
                        pointIt(pref, styleRefP)


## 	### scatter plot for comparison
## 	scax.scatter(alle[1], alle[2], marker = "x")


def postProcessSimple(fig, ax, labels):
        ### set labels
## 	print dir(ax)
        ax.set_xlabel(labels[0])
        ax.set_ylabel(labels[1])

        ### make borders invisible
        ax.patch.set_visible(False)
        fig.patch.set_visible(False)

## def postProcessScatter(fig, ax):
##         ### set labels
## ## 	print dir(ax)
##         ax.set_xlabel(sclabels[0])
##         ax.set_ylabel(sclabels[1])
## 
##         ### make borders invisible
##         ax.patch.set_visible(False)
##         fig.patch.set_visible(False)

def postProcessPic(fig, ax, tit):
        ### draw fixpoint
	pointIt(fp, styleFP, ax = ax)
	### add titles to the axes
        if titleit:
                ax.set_title(tit)
	if withPS:
## 		assert False, "construction site"
		maxcond = lambda x, y: x + y < anderies.C_0

		boundaries =  [0, 0, anderies.C_0, anderies.C_0]
##         axes = np.array([ boundaries[1],
##                         boundaries[3],
##                         boundaries[0],
##                         boundaries[2]])

		cDefault = "blue"
		styleDefault = {"linewidth": 8,
				"color":"#009fda",
				"arrowsize": 1.5}
		anderiesPS = lambda **kwargs: plotPhaseSpace(anderies.derivPS, boundaries,  maskByCond = maxcond, colorbar = False, steps = 30, alpha = 1, ax = ax, lwspeed = True,  **kwargs)
		anderiesPS(style = styleDefault)
## 		anderiesPS()
##                 plt.show()
##                 assert False


	### set borders correctly
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
        ### set labels
        ax.set_xlabel("$c_{terrestrial}$")
        ax.set_ylabel("$c_{marine}$")


        ### make borders invisible
##         ax.patch.set_visible(False)
##         fig.patch.set_visible(False)

def setTimeLabels():
        for tlabel, cb in zip(timeLabels, cbs):
                cb.set_label(tlabel)



def savePics(r, figs):
	for fig, name in zip(figs, picnames):
		savePlot("P%s%s%s%s.%s"%(picprefix, runname, name, r0suffix%r, picfilesuffix), fig = fig)

def saveDist(end = ""):
	savePlot("P%s%s%s%s.%s"%(picprefix, runname, distpicname, end, picfilesuffix), fig = distfig)

def saveScatter(end = ""):
	savePlot("P%s%s%s%s.%s"%(picprefix, runname, scpicname, end, picfilesuffix), fig = scfig)

def saveAllPics():
	for fig, name, r in zip(figs, picnames, r0Array):
		savePlot("P%s%s%s%s.%s"%(picprefix, runsuffix, name, r0suffix%r, picfilesuffix), fig = fig)
	savePlot("P%s%s%s.%s"%(picprefix, runsuffix, scpicname, picfilesuffix), fig = scfig)

def clearBetween():
	pass
## 	global axs, figs
## 	for ax, fig in zip(axs, figs):
## 		del ax
## 		del fig
## 	figs = [plt.figure(figsize=(12, 9)) for i in range(numfig)]
## 	axs = [fig.add_subplot(111) for fig in figs]
## 	[ax.cla() for ax in axs]

def singleEps(k):
## 	global axs, figs
	global cbs
        cbs = [] # filled during plotting
	clearBetween()
	figs = [plt.figure(figsize=(12, 9)) for i in range(numfig)]
	axs = [fig.add_subplot(111) for fig in figs]
	r = r0[k]
	names = map(lambda name: name + " $(\epsilon = %1.2e)$"%r, genericnames)
	plotData(x, y, (tAlls[:, :, k], trenAlls[:, :, k], intDists[:, :, k]), axs)
	[postProcessPic(fig, ax, tit) for fig, ax, tit in zip(figs, axs, names)]
	setTimeLabels()
	if saveit:
		savePics(r, figs)
	if showit:
		plt.show()
## 	else:
	plt.close("all")
	del axs
	del figs


def makeOptDict(optInp):
        opts = dict(optInp)
        for opt in opts.keys():
                x = 0
                if opt.startswith("-"):
                        x = 1
                if opt.startswith("--"):
                        x = 2
                opts[ opt[x:] ] = opts.pop( opt )
        return opts


avOpts = ["show", "save", "notitle", "ps", "scatter", "distro", "name="]

def issetOpt(opts, key):
        return key in opts.keys()



if __name__ == "__main__":
        optInp, args = getopt(sys.argv[1:], "", avOpts)
        opts = makeOptDict(optInp)

	# check for input

        runname = ""
        if issetOpt(opts, "name"):
                runname = opts["name"]
                print "runname:", runname
                print


        DATAFILE = "anderies%s.data"%runname


	showScatter = issetOpt(opts, "scatter")
	showDistro = issetOpt(opts, "distro")

	showit = issetOpt(opts, "show")
	saveit = issetOpt(opts, "save")
        titleit = not issetOpt(opts, "notitle")
	withPS = issetOpt(opts, "ps")

	loadFigs = (showit or saveit) and not showScatter and not showDistro
        anderies.alpha = 0.3


	THREADS = 4

	maskingRad = 5e-2
	

	fp = np.array([ 0.44226639,  0.2788668 ]) # for alpha 0.3

	x, y, r0, pref, tref, (tAlls, trenAlls, intDists) = loadData(DATAFILE)
## 	print np.shape(tAlls)
## 	alle = tAll, trenAll, intDist
	assert r0.ndim == 1
	numr0 = len(r0)

	picprefix = "Anderies"
	picfilesuffix = "png"
        remPerc = 1
	
	if loadFigs:
## 		cbs = []
		numfig = 3

## 		global axs
		genericnames = ["Time first entry", "renormalized Time ", "integrated Distance"]	
		timeLabels = ["absolute return time $t_{ret}$", r"Regularized Return Time $\Delta T$", "audic $D$"]

		picnames = ["Time1", "TimeRen", "DistInt"]
## 		r0suffix = "eps%1.1e"
		r0suffix = "eps%1e"

## 		assert len(figs) == len(genericnames) == len(picnames) 

		if not showit and THREADS != 1:
			p = mp.Pool(processes = THREADS)
			mapping = p.map
		else:
			mapping = map

		mapping(singleEps, range(numr0))
## 		mapping(singleEps, [numr0-1])

	if showScatter:
		scfig = plt.figure(figsize=(9, 9))
		scax = scfig.add_subplot(111) 
		scpicname = "Both"
		sclabels = [r"$\Delta t_{ret}$", r"$D$"]
		### scatter plot for comparison
		scax.scatter(trenAlls[:,:,-1], intDists[:, :, -1], marker = "x")
		postProcessSimple(scfig, scax, sclabels)
		if saveit:
			saveScatter()
		plt.yscale("log")
		if saveit:
			saveScatter(end = "log")
		plt.show()

	if showDistro:
                num = 2
		distfigs = [plt.figure(figsize=(9, 9)) for n in range(num)]
		distaxs = [fig.add_subplot(111) for fig in distfigs] 
		distpicname = ["TDist", "DDist"]
		distlabels = [[r"$\Delta t_{ret}$", r"%"], [r"$D$", r"%"]]
                
                for a, inputs in enumerate([trenAlls, intDists]):
                        dat = ma.compressed(inputs[:, :, -1])
                        low = np.percentile(dat, remPerc)
                        high = np.percentile(dat, 100 - remPerc)
                        dat = dat[ (dat > low) & (dat < high) ]
        ##                 print low, high

                        bins = np.linspace(np.min(dat), np.max(dat), 200)
                        n, bins, patches = distaxs[a].hist(dat, bins, normed=1, histtype='bar', rwidth=0.8)
                        postProcessSimple(distfigs[a], distaxs[a], distlabels[a])
		if saveit:
			saveDist()
		plt.show()


