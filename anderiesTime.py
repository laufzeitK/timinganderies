import numpy as np
from numpy import random as rand
from numpy import linalg as la
from numpy import ma

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patch

from scipy.integrate import odeint
from scipy import stats

from scipy.optimize import fsolve, curve_fit

import sys, os, time

from functools import partial

from getopt import gnu_getopt as getopt

import warnings as warn

from useful import *

# import the model code
import models.AnderiesTim as anderies

# my own module
# just used for the pres backup pix
from myPhaseSpace import savePlot


global tref, pref
tref = 0
pref = np.array([0.1, 0.5])

def drawCircle(p, r):
	phi = np.linspace(0, 2*np.pi, 100)
## 	print np.shape(phi)
	circ = np.empty((2,) + np.shape(phi))
	circ[0, :] = p[0] + r*np.sin(phi)
	circ[1, :] = p[1] + r*np.cos(phi)
	plt.plot(circ[0], circ[1])

def calcIntD(traj):
	return np.sum(la.norm(traj - fp, axis = 1))

def plotPresBackup(epses, ts, ylabel, title, fname, fitresult, save = True):
    
##     assert False
##     print fitresult
##     for ep, lep, t in zip(epses, np.log10(epses), eval_fit(np.log(epses), fitresult)):
##         print ep, lep, t
    plt.plot(epses, ts, "gx")
    plt.plot(epses, eval_fit(np.log10(epses), fitresult), "rx")
    plt.xscale("log")
    plt.xlabel("$\epsilon$")
    plt.ylabel(ylabel)
    plt.title(title)
    if save:
        savePlot(fname)
    plt.show()

def singleTimeRun(point, mode = "normal", alpha = None, newfp = None, withJacobian = False, verb = 1, showPlots = False, saveBackup = False):

        point = np.array(point)
        pointi = point
    
        if not alpha is None:
            anderies.alpha = alpha

        if not newfp is None:
            global fp
            fp = newfp

        if verb == 2:
                print "pointi", pointi

        # check whether integration is with or without (explicit) Jacobian
        # the pre key
        if withJacobian:
                integ = lambda traj, point, pre = +1: np.vstack((traj, odeint(anderies.deriv, point, pre*tArray, Dfun = anderies.Jfunc)))
        else:
                integ = lambda traj, point, pre = +1: np.vstack((traj, odeint(anderies.deriv, point, pre*tArray)))

        # check whether the point is too close
        timeshift = 0
        tooClose = fCond(pointi, maxeps, locfp = fp)
	if tooClose:
                # integrate backwards until a point outside of maxeps
                # so the linear approximation can be done from there
                # not optimal, but should be working
                warn.warn("\n%s is closer than maxeps to the fixpoint"%str(pointi), UserWarning, stacklevel = 2)
                traj = np.zeros((0, 2))
                for n in xrange(nmax):
                        traj = integ(traj, point, pre = -1)
                        point = traj[-1]
                        if not fCond(point, maxeps, locfp = fp):
                                break
                y = min(np.count_nonzero(~np.isnan(traj[:,0])), np.count_nonzero(~np.isnan(traj[:, 1])))
                traj = traj[0:y]
                x = np.count_nonzero(fCond(traj, maxeps, locfp = fp)) - 1
                timeshift = - n*trun - tArray[x%tnum]
                point = traj[x]
        ts = np.ones((k,)) * -1
        intDs = np.zeros((k,))
        traj = np.zeros((0, 2))
        for n in xrange(nmax):
                traj = integ(traj, point)

                point = traj[-1]

                if showPlots:
                        plt.close("all")
##                         plt.plot(traj[-tnum:,0], traj[-tnum:,1])
## 
##                         print n,"point", point, fp
##                         drawCircle(fp, mineps)
##                         plt.show()

                if fCond(point, mineps, locfp = fp):
                        break

        if traj.size != 0: # else, t = -1 and intD = 0 already set
                for a, eps in enumerate(epses):  # a = 0 .. k-1    
                        if fCond(point, eps, locfp = fp):
                                finalCond = fCond(traj, eps, locfp = fp)
                                index = np.sum(~finalCond) - n*tnum 

                                if not index < len(traj):
                                        warn.warn("\nindex for tArray[index] is too big for %s"%str(pointi), Userwarning, stacklevel = 2)
                                try:
                                    ts[a] = n*trun + tArray[index]
                                except IndexError, e:
                                    print
                                    print pointi, n
                                    print index, np.shape(tArray), np.shape(traj)
                                    print a, np.shape(ts)
                                    print type(e).__name__, ":", str(e), "for", repr(pointi)
                                    ts[a] = (n+1)*trun

                                finalTraj = traj[~finalCond,:]
                                intDs[a] = calcIntD(finalTraj) * dt

                        else:
				if withJacobian:
					###  t = -1 and intD = 0 already set
					warn.warn("\nmaximum reached for %s"%str(pointi), UserWarning, stacklevel = 2)
				else:
##                                     print "JAC"
                                    warn.warn("\n%s needs to be run with Jacobian"%str(pointi), UserWarning, stacklevel = 2)
                                    return singleTimeRun(pointi, mode = mode, withJacobian = True)
        if np.any(ts == -1):
                warn.warn("\nsomething didn't work with %s"%repr(pointi), UserWarning, stacklevel = 2)
                print pointi
                assert False

        # if the point was too close, the time needs to be shifted (if not, the value is 0)
        ts = ts + timeshift

        if mode == "ref":
            conv, pfitt  = fit_if_converged(logepses, ts)
            if not conv:
                raise RuntimeError("fit for reference time didn't converge")
##             tabs = eval_fit(logr0, pfitt)
            tabs = eval_ret(logr0, logepses, ts, conv, pfitt)
            ### make plots for presentation backup
            if showPlots:
                plotPresBackup(epses, ts, "$t_{ret}(x_{ref})$", "$x_{ref} = "+repr(pointi)+"$", "ConfirmTref.png", pfitt, save = saveBackup)
            return tabs, ts
        else:
            tds = ts - trefs
##                 print ts
            ### include errors in fit?
            convt, pfitt  = fit_if_converged(logepses, ts)
            convtd, pfittd = fit_if_converged(logepses, tds, tolimit = True)
            convD, pfitintD = fit_if_converged(logepses, intDs, tolimit = True)
##                 if showPlots:
##                     print "fit ts  ", pfitt
##                     print "fit tds ", pfittd
##                     print "fit intD", pfitintD
            tabs = eval_ret(logr0, logepses, ts, convt, pfitt)
            tren = eval_ret(logr0, logepses, tds, convtd, pfittd)
            intD = eval_ret(logr0, logepses, intDs, convD, pfitintD)
##             tabs = eval_fit(logr0, pfitt)
##             tren = eval_fit(logr0, pfittd)
##             intD = eval_fit(logr0, pfitintD)
            ### make plots for presentation backup
            if showPlots:
                if la.norm(pointi - np.array([0.6, 0.3])) < 0.05:
                    print
                    print pointi
                print "NOW! pointi", pointi
##                     plt.plot(logepses, eval_fit(logepses, pfitt), "rx")
##                     plt.show()
                plotPresBackup(epses, ts, " $t_{ret}(x)$", "$x = "+repr(pointi)+"$", "ConfirmTret.png", pfitt, save = saveBackup)
##                     plotPresBackup(epses, tds, "$\Delta t_{ret}(x)$", "$x = "+repr(pointi)+"$", "ConfirmDeltaTret.png", pfittd, save = saveBackup)
                print tabs, tren, intD
##                 assert False

##                 print tabs, tren, intD
##                 assert False
            return tabs, tren, intD

def fitF0(x, b, c, d):
##     return b + a*x + c/(d - x)
    return b + c*np.exp( d * x)

def fitF(x, a, b, c, d):
##     return b + a*x + c/(d - x)
    return b + a*x + c*np.exp( d * x)

def eval_ret(xvals, xs, ys, conv, fitresult):
    n = len(xvals)
    xvals = xvals
    yvals = np.empty(np.shape(xvals))
    for i in range(n):
        x = xvals[i]
        if x < maxleps and x > minleps:
            j = np.argmax( x > xs )
            assert j > 0
            yvals[i] = (ys[i] + ys[i-1])/2
        elif conv:
            yvals[i] = eval_fit(x, fitresult)
        else:
            yvals[i] = np.nan
    return yvals




def fit_if_converged(x, y, tolimit = False, verb = False):
    # why not fit with something like y = a + b*x + c*1/(x-d)
    # sensible approach by taking  the second order Jacobians as well
    # and look how the return and difference converges then
    if tolimit:
        n = 0
    else:
        n = 1

    weights = np.linspace(1, k, k)

    p = np.polyfit(x, y, n, w = weights)
    error = la.norm( eval_fit(x, p) - y)
    if verb:
        print "tolimit", tolimit
        print "p", p
        print "error", error,
    if error < linThresh:
        if verb:
            print "linear"
        return True, p
    if verb:
        print "non-linear"

    # else
    try:
        x1, y1 = x[0], y[0]
        k2 = int(k/8)
        if verb:
            print "k2", k2
        x2, y2 = x[k2], y[k2]

        if tolimit:
            b0 = np.average(y[-5:])
            c0 = np.exp(-x2/(x1-x2)) * (y1 - b0)
            d0 = 1/(x1-x2) * np.log( (y1-b0) / (y2-b0) )
            p0 = (b0, c0, d0)
            f = fitF0

        else:
            a0, b0 = np.polyfit(x[-5:], y[-5:], 1)
            c0 = np.exp(-x2/(x1-x2)) * (y1 - a0*x1 - b0)
            d0 = 1/(x1-x2) * np.log( (y1 - a0*x1 - b0) / (y2 - a0*x2 - b0) )

            p0 = (a0, b0, c0, d0)
##             p0 = (p[0], p[1], 0.01, 1)
            f = fitF
        if verb:
            print "p0", p0
        c = curve_fit(f, x, y, p0 = p0, sigma = 1/weights)[0]
        err = False
    except RuntimeError, e:
        return False, np.empty((0,))
    if verb:
        error2 = la.norm( eval_fit(x, c) - y)
        print "c", c
        print "error2", error2
        print "err", err
        plt.plot(x, y, "b*")
        plt.plot(x, eval_fit(x, c), "rx")
        plt.plot(x, eval_fit(x, p), "gx")
        plt.show()
    return True, c
##     return np.polyfit(x, y, 1)

def eval_poly(x, fitresult):
    n = len(fitresult) 
    v = 0
    for i in range(n):
        v += x ** (n - 1 - i) * fitresult[i]
    return v
##     return x * fitresult[0] + fitresult[1]

def eval_fit(x, fitresult):
    if len(fitresult) <= 2:
        return eval_poly(x, fitresult)
    elif len(fitresult) == 3:
        return fitF0(x, *fitresult)
    return fitF(x, *fitresult)

##     elif len(fitresult) == 4:
##         return fitF(x, *fitresult)
##     raise RuntimeError("somewhere something went wrong ... did you extend the code incompletely?")

### old stuff
## def fit_if_converged(x, y, n):
##     # why not fit with something like y = a + b*x + c*1/(x-d)
##     # sensible approach by taking  the second order Jacobians as well
##     # and look how the return and difference converges then
##     pfitt  = np.polyfit(x, y, n)
##     return pfitt
## 
## 
## def eval_fit(x, p):
##     return x * p[0] + p[1]

def fCond(p, eps, locfp = None):
    if locfp == None:
        locfp = fp
    return la.norm(p - locfp, axis = p.ndim - 1) < eps

def globalStuff():
	global tArray, dt, tmax, logepses, epses, mineps, maxeps, minleps, maxleps
	tArray = np.linspace(0, trun, tnum)
	dt = tArray[1] - tArray[0]
        tmax = nmax*trun

        logepses = np.linspace(logepsmax, logepsmin, k)
        epses = 10 ** logepses

        mineps = np.min(epses) 
        maxeps = np.max(epses) 
        minleps = np.min(logepses) 
        maxleps = np.max(logepses) 
	print "mineps", mineps
	print "maxeps", maxeps
	print "minleps", minleps
	print "maxleps", maxleps

def remapBack(tAll, trenAll, intDist, rets, invmask):
        for a, (i, j) in enumerate(np.transpose(invmask.nonzero())): # iterate over all non-masked elements
                tAll[i, j], trenAll[i, j], intDist[i, j] = rets[a]

def BasinRun():
	### create points for testing
	print "create points"
        print

	x = np.linspace(ctmin, ctmax , PerDim + 1) # +1 becuse in mx one is reduced again from the averaging
	y = np.linspace(cmmin, cmmax , PerDim + 1)

	mx = (x[:-1] + x[1:])*0.5
	my = (y[:-1] + y[1:])*0.5

	ps = ma.asarray(np.meshgrid(mx,my))

	retShape = np.shape(ps)[1:] + np.shape(r0)

        # create the mask (because the phase space is constrained)
        pmask = ps[0] + ps[1] > maskThresh
        fullmask = np.zeros(retShape, dtype = np.bool)
        fullmask[pmask] = True
        invpmask = ~pmask

        # prepare the return value arrays
	tAll = np.ma.masked_where(fullmask, np.zeros(retShape))
	trenAll = np.ma.masked_where(fullmask, np.zeros(retShape))
	intDist = np.ma.masked_where(fullmask, np.zeros(retShape))

	print "calculate reference time for regularization"
        print "pref", pref
        tref, trefs = singleTimeRun(pref, mode = "ref") 
        mpi.make_global({"trefs": trefs})
        print


        countmax = np.count_nonzero(invpmask)

	def iteratePoints(ps, invmask):
		for i, j in np.transpose(invmask.nonzero()): # iterate over all non-masked elements
			yield np.array([ps[:, i, j]])

        iterator = iteratePoints(ps, invpmask)

        rets = np.array(parallelInChunks(singleTimeRun, iterator, countmax, chunksize = CHUNK, verb = 1)) 

        print
	print "remap array to grid ...",

        # map the results after the parallelization back to the grid
        remapBack(tAll, trenAll, intDist, rets, invpmask)
        print "done"

        notConvMask = (tAll == -1)
        tAll = np.ma.masked_where(notConvMask, tAll)
        trenAll = np.ma.masked_where(notConvMask, trenAll)
        intDist = np.ma.masked_where(notConvMask, intDist)

	alle = [tAll, trenAll, intDist]
        saveData(DATAFILE, (x, y, r0, pref, tref, alle))



def createPoints(mode = "random"):
##     if mode == "random":

    # take care that the boundary condition is met
    # and that not too small values are chosen (because the whole thing is not stable then)
    relRange = maskThresh - minVal
    ps = rand.rand(2, Q) * relRange
    print "psmin", np.min(ps)
    print "psmax", np.max(ps)
    psCond = ps[0] + ps[1] > relRange
    ps[0, psCond] = relRange - ps[0, psCond]
    ps[1, psCond] = relRange - ps[1, psCond]
    ps += minVal
    print "after"
    print "psmin", np.min(ps)
    print "psmax", np.max(ps)
    print
    return ps
## 
##     print np.shape(ps)
## 
## 
## 
##     PerDim = 200
##     x = np.linspace(ctmin, ctmax , PerDim + 1) # +1 becuse in mx one is reduced again from the averaging
##     y = np.linspace(cmmin, cmmax , PerDim + 1)
## 
##     mx = (x[:-1] + x[1:])*0.5
##     my = (y[:-1] + y[1:])*0.5
## 
##     ps = ma.asarray(np.meshgrid(mx,my))
##     print np.shape(ps)
##     assert False
## 
## 
## def globalizeRefs(tr, trs):
##     global tref, trefs
##     tref = tr
##     trefs = trs
    

class SingleRun(object):
    def __init__(self):
        pass
    def __str__(self):
        txt = "SingleRun(" 
        for el in dir(self):
            if not el.startswith("__"):
                txt += " %s = "%el + str(eval("self."+el)) + ","
        txt +=" )"
        return txt
    def __repr__(self):
        txt = "SingleRun(" 
        for el in dir(self):
            txt += " %s = "%el + repr(eval(el)) + ","
        txt +=" )"
        return txt

def BasinRunAlphas():

        MList = [2,3,4]
        Mnum = len(MList)

	# create return arrays
	lena = len(alphas)
	Ts = np.zeros((lena, )) # regularized time
	Ds = np.zeros((lena, )) # integrated distance
	TMs = np.zeros((lena, Mnum)) # moments of Ts
	DMs = np.zeros((lena, Mnum)) # moments of Ds

        global fp
        tinit = time.time()
        for i, a in enumerate(alphas):
                anderies.alpha = a
		print 
		print "#"*40
		print
                print "alpha", a, "(%i/%i)"%(i+1, lena)
		print

                fp = fsolve(lambda p: anderies.deriv(p, 0), fp, factor = 0.1)

                print "found fp", fp
		print

		### create points for testing
		print "create points"
		print

## 		iterator = np.nditer(ps, flags = ["external_loop"], order = "F")
                def iteratePoints(ps, cmax):
                    for i in xrange(cmax): # iterate over all non-masked elements
                        yield np.array([ps[:, i]])

		ps = createPoints()
                ps[:, 0] = [ 0.37149707,  0.24401594]
                ps[:, 1] = [ 0.34524033,  0.26694514]
		iterator = iteratePoints(ps, Q)

		print "calculate reference time for regularization"
		print "pref", pref
                print

		tref, trefs = singleTimeRun(pref, mode = "ref") 
                mpi.make_global({"trefs": trefs})





                rets = np.array(parallelInChunks(singleTimeRun, iterator, Q, kwargslist = dict(newfp = fp, alpha = a), chunksize = CHUNK, verb = 1))[:,1:] # we don't actually need the absolute time 
                print
                FullData.append([a, ps, rets], tmpid = i)
                print

                print "calculate average and moments"
                Ts[i], Ds[i] = np.average(rets, axis = 0)
                for m in range(Mnum):
                        TMs[i, m], DMs[i, m] = stats.moment(rets, moment = MList[m], axis = 0)

                del rets # release the memory



		print "Ts[i]", Ts[i]
                for m in range(Mnum):
                        print "TMs[i, %i] %i"%(m, MList[m]), TMs[i, m]
                print 
		print "Ds[i]", Ds[i]
                for m in range(Mnum):
                        print "DMs[i, %i] %i"%(m, MList[m]), DMs[i, m]


	print "Ts"
	print Ts
	print "Ds"
	print Ds
        print
        print
        print "total running time for everything:",
        print secs2str(time.time() - tinit)

        saveData(DATAFILE, (Q, r0, alphas, MList, Ts, TMs, Ds, DMs))


## def makeOptDict(optInp):
##     opts = dict(optInp)
##     for opt in opts.keys():
##         x = 0
##         if opt.startswith("-"):
##             x = 1
##         if opt.startswith("--"):
##             x = 2
##         opts[ opt[x:] ] = opts.pop( opt )
##     return opts


def issetOpt(opts, key):
    return opts.isset(key)
##         return key in opts.keys()


## def help():
##     print "available Options are"
##     print avOpts

avOpts = ["alphas", "ps", "name=", "collectfiles"]

if __name__ == "__main__":

##     optInp, args = getopt(sys.argv[1:], "h", avOpts)
##     opts = makeOptDict(optInp)

    opts = OptDict(avOpts)

##     if issetOpt(opts, "h"):
##             help()
##             sys.exit(0)

    # check for input
    runmode = ""
    if issetOpt(opts, "alphas"):
        runmode = "alphas"
    elif issetOpt(opts, "ps"):
        runmode = "ps"
    
    assert runmode, opts.help()

##         print runalphas
##         assert False

    runname = ""
    if issetOpt(opts, "name"):
            runname = opts["name"]

## 	maxcond = lambda x, y: x + y < anderies.C_0

    # where to save the stuff

    if runmode == "ps":
        DATAFILE = "anderies%s.data"
        FULLDATAFILE = "anderies%s.data.full"
    elif runmode == "alphas":
        DATAFILE = "alphaAnderies%s.data"
        FULLDATAFILE = "alphaAnderies%s.data.full"
    else:
        assert False, "wrong runmode?"


    DATAFILE = DATAFILE%runname
    FULLDATAFILE = FULLDATAFILE%runname

    # how much should be run
    THREADS = mpi.n_procs

    # time stuff
    nmax = 20
    trun = 1e4
    tnum = 1e6

    # threshold for linearity (rms)
    linThresh = 0.8

    # threshold for the constraint: sum of stocks = C_0 
    minVal = 0.01
    maskThresh = anderies.C_0 - minVal
    assert maskThresh < anderies.C_0

    # boundaries of the system
    ctmin = 0
    ctmax = anderies.C_0
    cmmin = 0
    cmmax = anderies.C_0

    # for the fitting
    k = 40
    logepsmin = -6
    logepsmax = -3

    # set the necessary time variables globally (and other)
    globalStuff()

    # as starting fp use the alpha = 0.3 value
    fp = np.array([ 0.44226639,  0.2788668 ]) 

    parlist = ["runmode", "runname", "DATAFILE", "FULLDATAFILE", "THREADS", "CHUNK", "minVal", "maskThresh", "nmax", "trun", "tnum", "r0", "fp", "k", "logepsmax", "logepsmin"]
    if runmode == "ps":
        PerDim = 100
        r0 = np.logspace(-4, -50, 47)
        anderies.alpha = 0.3
        alphas = np.array([anderies.alpha])

        fp = fsolve(lambda p: anderies.deriv(p, 0), fp, factor = 0.1) # just if one changes alpha

        if THREADS != -1:
            CHUNK = max(int(PerDim/2), 1) * THREADS
        else:
            CHUNK = max(int(PerDim/2), 1) * 4

        parlistrun = ["PerDim"]

    elif runmode == "alphas":
        Q = 500
        # what alphas
        stopbef = 0.001
        alphamin = 0.387
##         alphamin = 0.3
        alphastep = 0.001
        alphaBif = anderies.alphaBifurc()
        alphas = np.arange(alphamin, alphaBif - stopbef, alphastep)

        r0 = np.array([1e-10])

        if THREADS != -1:
            CHUNK = max(int(np.sqrt(Q)/2), 1) * THREADS
        else:
            CHUNK = max(int(np.sqrt(Q)/2), 1) * 4

        parlistrun = ["Q", "alphamin", "alphastep", "stopbef", "alphaBif"]


    parlist.extend(parlistrun)


    logr0 = np.log10(r0)
    assert max(logr0) < logepsmax



    FullData = DataFile(FULLDATAFILE, tmpNum = len(alphas), keepTmps = mpi.am_slave)
    RunData = DataFile(DATAFILE, tmpNum = len(alphas), keepTmps = mpi.am_slave)

    
    mpi.slavesServeOnly()
    
    # from here one everything is only done by the master process

    FullData.clear()
    RunData.clear()


    print 
    print "--- Anderies' model parameters:"
    for el in ["C_0","a_f","b_f","a_p","b_r","b_p","c_p","c_r","a_r","b_T","a_T","beta","r_tc","k","alpha","a_m"]:
            print el, eval("anderies."+el)
    print
    print "--- computational parameters:"
    for el in parlist:
            print el, eval(el)
    print
    
    if runmode == "alphas":
        mpi.run(masterFunc = BasinRunAlphas)

    if runmode == "ps":
        mpi.run(masterFunc = BasinRun, verbose = False)




