
import numpy as np
from numpy import random as rand
from numpy import ma
import numpy.linalg as la

from scipy import stats

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.colors as cols

## from mpl_toolkits import mplot3d as plt3d

import sys, os

import warnings as warn
import multiprocessing as mp

import pickle
from useful import *

from getopt import gnu_getopt as getopt

# import Vera's code, by now quite a bit modified
import models.AnderiesTim as anderies

# my own module
from myPhaseSpace import plotPhaseSpace, savePlot, saveAx



mpl.rcParams["axes.labelsize"] = 36
mpl.rcParams["xtick.labelsize"] = 24
mpl.rcParams["ytick.labelsize"] = 24
mpl.rcParams["font.size"] = 36




#009FDA
styleFP = dict(markersize = 10, marker = "8", color = "#009FDA")
styleRefP = dict(markersize = 20, marker = "*", color = "#009FDA")


## cdict = {'red': ((0., 0.89, 0.89),
##                  (1, 0, 0)),
##          'green': ((0., 0.45, 0.45),
##                    (1, 0.62, 0.62)),
##          'blue': ((0., 0.13, 0.13),
##                   (1, 0.85, 0.85))}
## 
## cdict = {'red': ((0., 0.89, 0.89),
##                  (1, 0.41, 0.41)),
##          'green': ((0., 0.45, 0.45),
##                    (1, 0.57, 0.57)),
##          'blue': ((0., 0.13, 0.13),
##                   (1, 0.23, 0.23))}
## 
## cdict = {'red': ((0, 0.41, 0.41),
##                  (1, 0, 0)),
##          'green': ((0, 0.57, 0.57),
##                    (1, 0.62, 0.62)),
##          'blue': ((0, 0.23, 0.23),
##                  (1, 0.85, 0.85))}
## 
## PIKcmap = cols.LinearSegmentedColormap('PIKcolormap',cdict,256, gamma = 0.6)
PIKcmap = plt.get_cmap("hot")

def pointIt(xy, style, ax = plt):
        ax.plot([xy[0]],[xy[1]], **style)

## def loadData(fname):
## 	printit("loading data from %s ..."%fname , end = " ")
## 	with open(fname, "rb") as f:
## 		data = pickle.load(f)769,000 mm
## 	printit("done")
## 	return data
## 769,000 mm

def plotData(x, y, alle, r0axs):


	assert len(r0axs) == len(alle)
	l = len(r0axs)
	for i, ax, xAll in zip(range(l), r0axs, alle):
		plt.sca(ax)
		addkwarg = {}
		if i in [0]: # where the absolute times are measured
			addkwarg["vmin"] = 0
			addkwarg["vmax"] = np.percentile(xAll, 100 - remPerc)
                elif i in [1]: # where the time difference is measured
			addkwarg["vmin"] = np.percentile(xAll, remPerc)
			addkwarg["vmax"] = np.percentile(xAll, 100 - remPerc)
## 			mask = xAll < -50
## 			xAll.mask[ mask ] = True
## 			print dir(xAll)
		c = plt.pcolor(x,y,xAll, cmap = PIKcmap, **addkwarg)
		
		cb = plt.colorbar()
		cb.outline.set_visible(False)
                cbs.append(cb)

                if i in [1]:
                        pointIt(pref, styleRefP)


## 	### scatter plot for comparison
## 	scax.scatter(alle[1], alle[2], marker = "x")


def postProcessSimple(fig, ax, labels):
        ### set labels
## 	print dir(ax)
        ax.set_xlabel(labels[0])
        ax.set_ylabel(labels[1])

        ### make borders invisible
        ax.patch.set_visible(False)
        fig.patch.set_visible(False)

## def postProcessScatter(fig, ax):
##         ### set labels
## ## 	print dir(ax)
##         ax.set_xlabel(sclabels[0])
##         ax.set_ylabel(sclabels[1])
## 
##         ### make borders invisible
##         ax.patch.set_visible(False)
##         fig.patch.set_visible(False)

def postProcessPic(fig, ax, tit):
        ### draw fixpoint
	pointIt(fp, styleFP, ax = ax)
	### add titles to the axes
        if titleit:
                ax.set_title(tit)
	if withPS:
		assert False, "construction site"
		maxcond = lambda x, y: x + y < anderies.C_0

		boundaries =  [0, 0, anderies.C_0, anderies.C_0]
##         axes = np.array([ boundaries[1],
##                         boundaries[3],
##                         boundaries[0],
##                         boundaries[2]])

		cDefault = "blue"
		styleDefault = {"linewidth": 3,
				"color":cDefault,
				"arrowsize": 1.5}
		anderiesPS = lambda **kwargs: plotPhaseSpace(anderies.deriv, boundaries,  maskByCond = maxcond, colorbar = False, steps = 30, invertAxes = True, alpha = 0.5, **kwargs)
		anderiesPS(style = styleDefault)


	### set borders correctly
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
        ### set labels
        ax.set_xlabel("$c_{terrestrial}$")
        ax.set_ylabel("$c_{marine}$")


        ### make borders invisible
##         ax.patch.set_visible(False)
##         fig.patch.set_visible(False)

def setTimeLabels():
        for tlabel, cb in zip(timeLabels, cbs):
                cb.set_label(tlabel)



def savePics(r, figs):
	for fig, name in zip(figs, picnames):
		savePlot("P%s%s%s%s.%s"%(picprefix, runname, name, r0suffix%r, picfilesuffix), fig = fig)

def saveDist(end = ""):
	savePlot("P%s%s%s%s.%s"%(picprefix, runname, distpicname, end, picfilesuffix), fig = distfig)

def saveScatter(end = ""):
	savePlot("P%s%s%s%s.%s"%(picprefix, runname, scpicname, end, picfilesuffix), fig = scfig)

def saveAllPics():
	for fig, name, r in zip(figs, picnames, r0Array):
		savePlot("P%s%s%s%s.%s"%(picprefix, runsuffix, name, r0suffix%r, picfilesuffix), fig = fig)
	savePlot("P%s%s%s.%s"%(picprefix, runsuffix, scpicname, picfilesuffix), fig = scfig)

def clearBetween():
	pass
## 	global axs, figs
## 	for ax, fig in zip(axs, figs):
## 		del ax
## 		del fig
## 	figs = [plt.figure(figsize=(12, 9)) for i in range(numfig)]
## 	axs = [fig.add_subplot(111) for fig in figs]
## 	[ax.cla() for ax in axs]

def singleEps(k):
## 	global axs, figs
	global cbs
        cbs = [] # filled during plotting
	clearBetween()
	figs = [plt.figure(figsize=(12, 9)) for i in range(numfig)]
	axs = [fig.add_subplot(111) for fig in figs]
	r = r0[k]
	names = map(lambda name: name + " $(\epsilon = %1.2e)$"%r, genericnames)
	plotData(x, y, (tAlls[:, :, k], trenAlls[:, :, k], intDists[:, :, k]), axs)
	[postProcessPic(fig, ax, tit) for fig, ax, tit in zip(figs, axs, names)]
	setTimeLabels()
	if saveit:
		savePics(r, figs)
	if showit:
		plt.show()
## 	else:
	plt.close("all")
	del axs
	del figs






if __name__ == "__main__":

    avOpts = ["show", "save", "notitle", "name="]
    opts = OptDict(avOpts)


    runname = ""
    if opts.isset("name"):
        runname = opts["name"]
        print "runname:", runname
        print


    FULLDATAFILE = "alphaAnderies%s.data.full"%runname


    titleit = not opts.isset("notitle")

    loadFigs = (opts.isset("show") or opts.isset("save")) 

    assert loadFigs

    maskingRad = 5e-2
    
    MList = [2,3,4]
    Mnum = len(MList)
    numbins = 200

    fp = np.array([ 0.44226639,  0.2788668 ]) # for alpha 0.3

## 	x, y, r0, pref, tref, (tAlls, trenAlls, intDists) = loadData(DATAFILE)
    loaded =  loadData(FULLDATAFILE)
##         FullData.append([a, ps, rets], tmpid = i)
    

    lena = len(loaded)

    assert lena, "file is empty"

    pshape = np.shape(loaded[0][1])
    rshape = np.shape(loaded[0][2])
    
    alphas = np.empty((lena, ))
    ps = np.empty((lena, ) + pshape)
    rets = np.zeros((lena, ) + rshape)

    # maybe work with generator?
    for i, (a, p, r) in enumerate(loaded):
        alphas[i] = a
        ps[i] = p
        rets[i] = r

    del loaded


    Ts = np.zeros((lena, )) # regularized time
    Ds = np.zeros((lena, )) # integrated distance
    TMs = np.zeros((lena, Mnum)) # moments of Ts
    DMs = np.zeros((lena, Mnum)) # moments of Ds


    skewT = np.zeros((lena, )) # skewness
    skewD = np.zeros((lena, )) # skewness
    kurtosisT = np.zeros((lena, )) # kurtosis
    kurtosisD = np.zeros((lena, )) # kurtosis
    varT = np.zeros((lena, )) # variation

    betasT = np.zeros((lena, )) # coefficient of bimodality
    betasD = np.zeros((lena, )) # coefficient of bimodality

    print "calculate average and moments"

    minT = np.min(rets[:, :, 0])
    maxT = np.max(rets[:, :, 0])
    binsT = np.linspace(minT, maxT, numbins)
    centerT = (binsT[:-1] + binsT[1:]) / 2
    widthbarT = 0.8*(binsT[1] - binsT[0])

    minD = np.min(rets[:, :, 1])
    maxD = np.max(rets[:, :, 1])
    binsD = np.linspace(minD, maxD, numbins)
    centerD = (binsD[:-1] + binsD[1:]) / 2
    widthbarD = 0.8*(binsD[1] - binsD[0])

##     print np.shape(binsT), np.shape(centerT)


    histsT = np.empty((numbins-1, lena))
    histsD = np.empty((numbins-1, lena))

    for i in range(lena):
        Ts[i], Ds[i] = np.average(rets[i], axis = 0)

        for m in range(Mnum):
            TMs[i, m], DMs[i, m] = stats.moment(rets[i], moment = MList[m], axis = 0)

        skewT[i], skewD[i] = stats.skew(rets[i], axis = 0)
        kurtosisT[i], kurtosisD[i] = stats.kurtosis(rets[i], axis = 0)
##         hists.append(np.histogram(rets[i, :, 0], bins = bins, density = True)[0])
        histsT[:, i] = np.histogram(rets[i, :, 0], bins = binsT, density = True)[0]
        histsD[:, i] = np.histogram(rets[i, :, 1], bins = binsD, density = True)[0]
        varT[i] = stats.variation(rets[i, :, 0], axis = 0)


        assert MList == [2,3,4]
        betasT[i] = (skewT[i] ** 2 + 1) / kurtosisT[i]

##         print
##         print "Ts[i]", Ts[i]
##         for m in range(Mnum):
##             print "TMs[i, %i] %i"%(m, MList[m]), TMs[i, m]
##         print 
##         print "Ds[i]", Ds[i]
##         for m in range(Mnum):
##             print "DMs[i, %i] %i"%(m, MList[m]), DMs[i, m]
## 
##     print


##     c = plt.pcolor(x,y,xAll, cmap = PIKcmap, **addkwarg)

    ### here

    # use PIK orange
    styleNormal = dict(markersize = 10, markeredgewidth = 3, marker = "x", color = "#E37222", linewidth = 3)

##     fig = plt.figure(figsize=(12, 9))
##     ax = fig.add_subplot(111)
##     plt.plot(alphas, Ds, **styleNormal)
##     ax.set_xlabel(r"$\alpha$")
##     ax.set_ylabel(r"$Avg(D)$")
##     if opts.isset("save"):
##         savePlot("DMAvg.jpg")
##     
##     fig = plt.figure(figsize=(12, 9))
##     ax = fig.add_subplot(111)
##     plt.plot(alphas, Ts, **styleNormal)
##     ax.set_xlabel(r"$\alpha$")
##     ax.set_ylabel(r"$Avg(\Delta T)$")
##     if opts.isset("save"):
##         savePlot("MAvg.jpg")
##     
##     for m in [0]:#range(Mnum):
##         fig = plt.figure(figsize=(12, 9))
##         ax = fig.add_subplot(111)
##         plt.plot(alphas, TMs[:, m], **styleNormal)
##         ax.set_xlabel(r"$\alpha$")
##         ax.set_ylabel(r"$Mom_%i(\Delta T)$"%MList[m] )
## ##         ax.set_ylabel(r"$Mom_%i(\Delta T)\ [s^%i]$"%((MList[m],) * 2) )
##         if opts.isset("save"):
##             savePlot("Mom%i.jpg"%(MList[m]))
##     
##         fig = plt.figure(figsize=(12, 9))
##         ax = fig.add_subplot(111)
##         plt.plot(alphas, DMs[:, m], **styleNormal)
##         ax.set_xlabel(r"$\alpha$")
##         ax.set_ylabel(r"$Mom_%i(D)$"%MList[m])
##         if opts.isset("save"):
##             savePlot("DMom%i.jpg"%(MList[m]))
    
##     fig = plt.figure(figsize=(12, 9))
##     ax = fig.add_subplot(111)
##     plt.plot(alphas, skewT, **styleNormal)
##     ax.set_xlabel(r"$\alpha$")
##     ax.set_ylabel(r"$skew(\Delta T)$")
## 
##     fig = plt.figure(figsize=(12, 9))
##     ax = fig.add_subplot(111)
##     plt.plot(alphas, kurtosisT, **styleNormal)
##     ax.set_xlabel(r"$\alpha$")
##     ax.set_ylabel(r"$kurtosis(\Delta T)$")
##     
##     fig = plt.figure(figsize=(12, 9))
##     ax = fig.add_subplot(111)
##     plt.plot(alphas, betasT, **styleNormal)
##     ax.set_xlabel(r"$\alpha$")
##     ax.set_ylabel(r"$\beta(\Delta T)$")
## 
##     fig = plt.figure(figsize=(12, 9))
##     ax = fig.add_subplot(111)
##     plt.plot(alphas, varT, **styleNormal)
##     ax.set_xlabel(r"$\alpha$")
##     ax.set_ylabel(r"coeff of variation $(\Delta T)$")

##     dalpha = alphas[1] - alphas[0]
##     malphas = np.linspace(alphas[0] - dalpha/2, alphas[-1] + dalpha/2, lena+1)   
##     dbin = bins[1] - bins[0]
##     mbins = np.linspace(bins[0] - dbin/2, bins[-1] + dbin/2, numbins+1)   

    
##     cmaps = sorted(m for m in plt.cm.datad if not m.endswith("_r"))
##     print len(cmaps)
##     for cmap in cmaps:
    fig = plt.figure(figsize=(12, 9))
    ax = fig.add_subplot(111)
    c = plt.pcolor(alphas, binsT, histsT, cmap = plt.get_cmap("hot_r"))
    cb = plt.colorbar()
    cb.outline.set_visible(False)
    cb.set_label(r"percentage")
    ax.set_xlabel(r"$\alpha$")
    ax.set_ylabel(r"$\Delta T$")
    plt.axis([alphas[-1], alphas[0], minT, maxT])
    if opts.isset("save"):
        savePlot("ColorRegTime.jpg")

    fig = plt.figure(figsize=(12, 9))
    ax = fig.add_subplot(111)
    c = plt.pcolor(alphas,binsD, histsD, cmap = plt.get_cmap("hot_r"))
    cb = plt.colorbar()
    cb.outline.set_visible(False)
    cb.set_label(r"percentage")
    ax.set_xlabel(r"$\alpha$")
    ax.set_ylabel(r"$D$")
    plt.axis([alphas[-1], alphas[0], minD, maxD])
    if opts.isset("save"):
        savePlot("ColorAUDIC.jpg")
##     ax.set_title(cmap)
##     ax.set_axes([alphas[0], alphas[-1]
##     PIKcmap = plt.get_cmap("hot")

##     maxP = np.max(hists)
##     for i in range(0, lena, lena/10):
##         fig = plt.figure(figsize=(12, 9))
##         ax = fig.add_subplot(111)
##         plt.bar(center, hists[i], width = widthbar, align = "center")
##         plt.axis([minT, maxT, 0, maxP])
##         ax.set_xlabel(r"$\Delta T$")
##         ax.set_ylabel(r"percent at $\alpha = %1.3f$"%alphas[i])
##         if opts.isset("save"):
##             savePlot("hist-alpha=%1.3f.jpg"%alphas[i])

    

    if opts.isset("show"):
        plt.show()
    


