
import numpy as np
from numpy import random as rand
import numpy.linalg as la

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.colors as cols

import sys, os

import warnings as warn

import pickle
from useful import printit, loadData

from getopt import gnu_getopt as getopt

# import Vera's code, by now quite a bit modified
import models.AnderiesTim as anderies

# my own module
from myPhaseSpace import plotPhaseSpace, savePlot, saveAx



## mpl.rcParams["axes.labelsize"] = 36
mpl.rcParams["xtick.labelsize"] = 24
mpl.rcParams["ytick.labelsize"] = 24
mpl.rcParams["font.size"] = 36




# use PIK orange
styleNormal = dict(markersize = 10, markeredgewidth = 3, marker = "x", color = "#E37222", linewidth = 3)
## styleRefP = dict(markersize = 10, marker = "*", color = "#E37222")


## cdict = {'red': ((0., 0.89, 0.89),
##                  (1, 0, 0)),
##          'green': ((0., 0.45, 0.45),
##                    (1, 0.62, 0.62)),
##          'blue': ((0., 0.13, 0.13),
##                   (1, 0.85, 0.85))}
## 
## cdict = {'red': ((0., 0.89, 0.89),
##                  (1, 0.41, 0.41)),
##          'green': ((0., 0.45, 0.45),
##                    (1, 0.57, 0.57)),
##          'blue': ((0., 0.13, 0.13),
##                   (1, 0.23, 0.23))}
## 
## cdict = {'red': ((0, 0.41, 0.41),
##                  (1, 0, 0)),
##          'green': ((0, 0.57, 0.57),
##                    (1, 0.62, 0.62)),
##          'blue': ((0, 0.23, 0.23),
##                  (1, 0.85, 0.85))}
## 
## PIKcmap = cols.LinearSegmentedColormap('PIKcolormap',cdict,256, gamma = 0.6)

def pointIt(xy, style, ax = plt):
        ax.plot([xy[0]],[xy[1]], **style)

## def loadData(fname):
## 	printit("loading data from %s ..."%fname , end = " ")
## 	with open(fname, "rb") as f:
## 		data = pickle.load(f)
## 	printit("done")
## 	return data

def plotMomenta(alphas, MList, TMs, DMs):
	for i, Vs in enumerate([Ts, Ds]):
		plt.sca(axs[i])
		plt.plot(alphas, Vs, **styleNormal)


def plotData(alphas, *allVals):
	for i, Vs in enumerate(allVals):
		plt.sca(axs[i])
		plt.plot(alphas, Vs, **styleNormal)


def postProcessPic(fig, ax, tit):
	### add titles to the axes
        if titleit:
                ax.set_title(tit)
	### set logarithmic scale
## 	ax.set_yscale(r"log")
        ### set labels
        ax.set_xlabel(r"$\alpha$")


        ### make borders invisible
##         ax.patch.set_visible(False)
##         fig.patch.set_visible(False)

def setYLabels():
##         ax.set_ylabel("$c_{marine}$")
        for ylabel, ax in zip(yLabels, axs):
                ax.set_ylabel(ylabel)



def saveAllPics():
	for fig, name in zip(figs, picnames):
		savePlot("%s%s%s.%s"%(picprefix, runsuffix, name, picfilesuffix), fig = fig)

def issetOpt(opts, key):
        return key in opts.keys()


if __name__ == "__main__":
        opts, args = getopt(sys.argv[1:], "", ["show", "save", "notitle", "name="])
        opts = dict(opts)

	# check for input

        runname = ""
        if issetOpt(opts, "--name"):
                runname = opts["--name"]
                print "runname:", runname
                print


        DATAFILE = "alphaAnderies%s.data"%runname

	data =  loadData(DATAFILE, verb = True)
	if len(data) == 5:
		Q, eps, alphas, Ts, Ds = data
	else:
		Q, eps, alphas, MList, Ts, TMs, Ds, DMs = data


	showit = issetOpt(opts, "--show")
	saveit = issetOpt(opts, "--save")
	titleit = not issetOpt(opts, "--notitle")

	Mnum = len(MList)


	fp = np.array([ 0.44226639,  0.2788668 ]) # for alpha 0.3

        print "prepare figures and axes"
## 	num = 2 * (Mnum + 1)
	num = 2 
	figs = [plt.figure(figsize=(12, 9)) for i in range(num)]
	axs = [fig.add_subplot(111) for fig in figs]
## 	names = ["$\Delta t_{ret}$", "$D$"]
	names = ["$\Delta t_{ret}$", "$D$"]
	yLabels = [r"$avg(\Delta t_{ret})$", " $avg(D)$"]

	picprefix = "Anderies"
	picnames = ["T", "D"]
	picfilesuffix = "png"

	assert len(figs) == len(axs) == len(names) == len(picnames) 


	
	plotData(alphas, Ts, Ds)
## 	plotMomenta(alphas, MList, TMs, DMs):

	### add titles to the axes
	[postProcessPic(fig, ax, tit) for fig, ax, tit in zip(figs, axs, names)]
        setYLabels()


	if saveit:
		saveAllPics()
	if showit:
		plt.show()
